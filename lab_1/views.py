from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Puteri Khatya Fahira' # TODO Implement this

# Create your views here.
def index(request):
    mhs_umur = calculate_age(1999)
    response = {'name': mhs_name, 'umur': mhs_umur}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return (date.today().year - birth_year)
